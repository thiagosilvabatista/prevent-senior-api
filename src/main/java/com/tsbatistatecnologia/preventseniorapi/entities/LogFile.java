package com.tsbatistatecnologia.preventseniorapi.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "log_file")
public class LogFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "num_id")
    private Long numId;

    @Column(name = "date_data")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime dateData;

    @Column(name = "str_ip")
    private String strIp;

    @Column(name = "str_request")
    private String strRequest;

    @Column(name = "num_status")
    private Integer numStatus;

    @Column(name = "str_user_agent")
    private String strUserAgent;

    public LogFile (final LocalDateTime dateData, final String strIp, final String strRequest, final Integer numStatus, final String strUserAgent){
        this.dateData = dateData;
        this.strIp = strIp;
        this.strRequest = strRequest;
        this.numStatus = numStatus;
        this.strUserAgent = strUserAgent;
    }

    public LogFile(){}
}
