package com.tsbatistatecnologia.preventseniorapi.repositories;

import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface LogFileRepository extends JpaRepository<LogFile, Long>, JpaSpecificationExecutor<LogFile> {

    @Transactional(readOnly = true)
    @Query(value = "select l from LogFile l where l.dateData BETWEEN :startDate and :endDate and l.strIp = :strIp")
    Optional<List<LogFile>> findLogsBetweenDatesAndIp(@Param("strIp") String strIp,
                                                      @Param("startDate") LocalDateTime startDate,
                                                      @Param("endDate") LocalDateTime endDate);

    @Transactional(readOnly = true)
    @Query(value = "select l from LogFile l where l.strIp = :strIp")
    Optional<List<LogFile>> findLogsByIp(@Param("strIp") String strIp);

}
