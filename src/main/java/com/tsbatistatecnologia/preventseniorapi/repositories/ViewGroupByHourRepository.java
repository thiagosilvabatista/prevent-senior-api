package com.tsbatistatecnologia.preventseniorapi.repositories;

import com.tsbatistatecnologia.preventseniorapi.dto.views.ViewGroupByHour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface ViewGroupByHourRepository extends JpaRepository<ViewGroupByHour, Long> {

    @Transactional(readOnly = true)
    @Query(value = "select v from ViewGroupByHour v where v.strIp = :strIp")
    Optional<List<ViewGroupByHour>> groupByLogFileByHour(@Param("strIp") String strIp);

}
