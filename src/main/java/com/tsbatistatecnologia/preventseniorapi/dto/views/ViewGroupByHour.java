package com.tsbatistatecnologia.preventseniorapi.dto.views;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "group_by_hour")
public class ViewGroupByHour {

    @Id
    @Column(name = "num_row")
    private Long numRow;

    @Column(name = "count")
    private Integer numCount;

    @Column(name = "date_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime dateDate;

    @Column(name = "str_ip")
    private String strIp;

    @Column(name = "str_user_agent")
    private String strUserAgent;

}
