package com.tsbatistatecnologia.preventseniorapi.services;

import com.tsbatistatecnologia.preventseniorapi.dto.views.ViewGroupByHour;
import com.tsbatistatecnologia.preventseniorapi.exceptions.LogFileException;
import com.tsbatistatecnologia.preventseniorapi.repositories.ViewGroupByHourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ViewGroupByHourService {

    @Autowired
    private ViewGroupByHourRepository viewGroupByHourRepository;

    public Optional<List<ViewGroupByHour>> groupByLogFileByHour(final String strIp){
        try{
            return viewGroupByHourRepository.groupByLogFileByHour(strIp);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }

    }

}
