package com.tsbatistatecnologia.preventseniorapi.services;

import com.google.common.base.Strings;
import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import com.tsbatistatecnologia.preventseniorapi.exceptions.LogFileException;
import com.tsbatistatecnologia.preventseniorapi.repositories.LogFileRepository;
import com.tsbatistatecnologia.preventseniorapi.utils.FileStorageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.criteria.Predicate;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class LogFileService {

    @Autowired
    private FileStorageUtils fileStorageUtils;

    @Autowired
    private LogFileRepository logRepository;

    public List<LogFile> saveLogInBatch (final MultipartFile multipartFile){

        try {

            final File file = fileStorageUtils.save(multipartFile.getOriginalFilename(), multipartFile.getInputStream());

            FileReader read = new FileReader(file.getPath());
            BufferedReader reader = new BufferedReader(read);

            List<LogFile> logFiles = new ArrayList<LogFile>();
            String line;
            while( (line = reader.readLine()) != null ){
                logFiles.add(createLogFile(line));
            }
            logRepository.saveAll(logFiles);
            return logFiles;

        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        } catch (IOException io) {
            throw  new LogFileException(io.getMessage());
        }



    }

    private LogFile createLogFile(final String lineFile){

        String[] lineData = lineFile.split("\\|");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        final LogFile logFile =  new LogFile(LocalDateTime.parse(lineData[0], formatter), lineData[1],
                                                                 lineData[2].replaceAll("\"",""), Integer.parseInt(lineData[3]),
                                                                 lineData[4].replaceAll("\"",""));
        return logFile;
    }

    public List<LogFile> findLogsBetweenDatesAndIp(final String strIp, final String startDate, final String endDate){
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            List<LogFile> list = logRepository.findAll((root, query, cb) -> {

                List<Predicate> predicates = new ArrayList<>();

                if (!Strings.isNullOrEmpty(strIp)) {
                    predicates.add(cb.equal(root.get("strIp"), strIp));
                }

                if (!Strings.isNullOrEmpty(startDate)) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("dateData"), LocalDateTime.parse(startDate, formatter)));
                }

                if (!Strings.isNullOrEmpty(endDate)) {
                    predicates.add(cb.lessThanOrEqualTo(root.get("dateData"),  LocalDateTime.parse(endDate, formatter)));
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            }).stream().collect(Collectors.toList());

            return list;

        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public List<LogFile> findLogsByIp(final String strIp){
        try {
            return logRepository.findLogsByIp(strIp).orElse(null);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public Optional<LogFile> findById(final Long numId){
        try {
            return logRepository.findById(numId);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public LogFile saveLog(final LogFile logFile){
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
            String dateNow = LocalDateTime.now().toString().substring(0, 23).replace("T"," ");
            logFile.setDateData(LocalDateTime.parse(dateNow, formatter));
            return logRepository.save(logFile);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public List<LogFile> saveAllLogs(final List<LogFile> logFiles){
        try {
            return logRepository.saveAll(logFiles);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public void delete(final Long numId){
        try{
            logRepository.deleteById(numId);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }
    }

    public LogFile update(final LogFile logFile){
        try{
            return logRepository.save(logFile);
        }catch (LogFileException e) {
            throw  new LogFileException(e.getMessage());
        }

    }

}
