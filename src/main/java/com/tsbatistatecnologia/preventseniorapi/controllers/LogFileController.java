package com.tsbatistatecnologia.preventseniorapi.controllers;

import com.tsbatistatecnologia.preventseniorapi.dto.views.ViewGroupByHour;
import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import com.tsbatistatecnologia.preventseniorapi.services.LogFileService;
import com.tsbatistatecnologia.preventseniorapi.services.ViewGroupByHourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/log-file")
@CrossOrigin(origins = "*")
public class LogFileController {

    @Autowired
    private LogFileService logFileService;
    @Autowired
    private ViewGroupByHourService viewGroupByHourService;

    @PostMapping(value = "/import")
    public ResponseEntity<List<LogFile>> saveLogs(@RequestParam("file") final MultipartFile multipartFile) throws IOException {
        return new ResponseEntity<List<LogFile>>(logFileService.saveLogInBatch(multipartFile), HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity<LogFile> saveLog(@RequestBody @Valid final LogFile logFile) {
        return new ResponseEntity<LogFile>(logFileService.saveLog(logFile), HttpStatus.CREATED);

    }

    @GetMapping(value = "/filter")
    public ResponseEntity<List<LogFile>> findLogsBetweenDatesAndIp(@RequestParam(value = "strIp") final Optional<String> strIp,
                                                              @RequestParam(value = "startDate") final Optional<String> startDate,
                                                              @RequestParam(value = "endDate") final Optional<String> endDate){
        return new ResponseEntity<List<LogFile>>(logFileService.findLogsBetweenDatesAndIp(strIp.orElse(null),
                                                                                          startDate.orElse(null),
                                                                                          endDate.orElse(null)
                                                                                        ), HttpStatus.OK);
    }

    @GetMapping(value = "/{numId}")
    public ResponseEntity<LogFile> findById(@PathVariable final Long numId){
        return new ResponseEntity<LogFile>(logFileService.findById(numId).orElse(null), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{numId}")
    public ResponseEntity<String> deleteLogFile(@PathVariable final Long numId) {
        logFileService.delete(numId);
        return new ResponseEntity<String>("Log deletado com sucesso.", HttpStatus.OK);

    }

    @PutMapping
    public ResponseEntity<LogFile> updateLogFile(@RequestBody @Valid final LogFile logFile) {
        return new ResponseEntity<LogFile>(logFileService.update(logFile), HttpStatus.OK);
    }

    @GetMapping(value = "/group-by-hour")
    public ResponseEntity<List<ViewGroupByHour>> groupByLogFileByHour(@RequestParam(value = "strIp") final Optional<String> strIp) {
        return new ResponseEntity<List<ViewGroupByHour>>(viewGroupByHourService.groupByLogFileByHour(strIp.orElse(null)).orElse(null), HttpStatus.OK);
    }

}
