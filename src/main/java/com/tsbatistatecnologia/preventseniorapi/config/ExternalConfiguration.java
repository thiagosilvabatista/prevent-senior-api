package com.tsbatistatecnologia.preventseniorapi.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Getter
@Configuration
public class ExternalConfiguration {

    private String logFilePath;

    ExternalConfiguration(@Value("${log-file-path}") final String logFilePath){
        this.logFilePath = logFilePath;
    }

    public String getRelativePathByStoragePath(final String fileName) {
        return getLogFilePath() + File.separator + fileName;
    }

}
