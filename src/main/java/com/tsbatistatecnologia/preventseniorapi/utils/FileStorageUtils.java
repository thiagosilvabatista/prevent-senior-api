package com.tsbatistatecnologia.preventseniorapi.utils;

import com.tsbatistatecnologia.preventseniorapi.config.ExternalConfiguration;
import com.tsbatistatecnologia.preventseniorapi.exceptions.LogFileException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
@RequiredArgsConstructor
public class FileStorageUtils {

    private static final int BUFFER_100_MB = 102400 * 1024;

    @Autowired
    private ExternalConfiguration externalConfiguration;

    public File save(final String fileName, final InputStream fileInputStream) {
        final File targetFile = new File(externalConfiguration.getRelativePathByStoragePath(fileName));
        try (final OutputStream outStreamFile = new FileOutputStream(targetFile)) {
            byte[] buffer = new byte[BUFFER_100_MB];
            int bytesRead;

            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                outStreamFile.write(buffer, 0, bytesRead);
            }

        } catch (final IOException e) {
            throw new LogFileException("Unable to save file");
        }
        return targetFile;
    }

}
