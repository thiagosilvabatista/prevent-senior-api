package com.tsbatistatecnologia.preventseniorapi.exceptions;

import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogFileException extends RuntimeException {

    private static final Long serialVersionUID = 1L;
    private LogFile logFile;

    public LogFileException (final String cause, LogFile logFile) {
        super(cause);
        this.logFile = logFile;
        log.error(cause, logFile);
    }

    public LogFileException (final String cause) {
        super(cause);
        log.error(cause);
    }

}
