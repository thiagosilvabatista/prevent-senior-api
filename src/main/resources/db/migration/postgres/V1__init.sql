CREATE TABLE log_file
(
    num_id serial NOT NULL,
    date_data timestamp NOT NULL,
    str_ip varchar(20) NOT NULL,
    str_request varchar(20) NOT NULL,
    num_status smallint NOT NULL,
    str_user_agent varchar(250) NOT NULL
)
;
ALTER TABLE log_file ADD CONSTRAINT pk_log
 PRIMARY KEY (num_id)
;
