create or replace view group_by_hour as
select
    date_trunc('hour', date_data) as date_date, -- or hour, day, week, month, year
    count(1),
    str_ip,
    str_user_agent,
    ROW_NUMBER () OVER (ORDER BY str_ip) as num_row
from
    log_file
group by
    1,
    str_ip,
    str_user_agent
;