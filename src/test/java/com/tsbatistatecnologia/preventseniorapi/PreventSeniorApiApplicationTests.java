package com.tsbatistatecnologia.preventseniorapi;

import com.tsbatistatecnologia.preventseniorapi.controllers.LogFileControllerTest;
import com.tsbatistatecnologia.preventseniorapi.repositories.LogFileRepositoryTest;
import com.tsbatistatecnologia.preventseniorapi.services.LogFileServiceTest;
import com.tsbatistatecnologia.preventseniorapi.utils.FileStorageUtilsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({LogFileControllerTest.class,
		             LogFileServiceTest.class,
		             LogFileRepositoryTest.class,
		             FileStorageUtilsTest.class})
public class PreventSeniorApiApplicationTests { }
