package com.tsbatistatecnologia.preventseniorapi.utils;


import com.tsbatistatecnologia.preventseniorapi.config.ExternalConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class FileStorageUtilsTest {

    @Autowired
    private FileStorageUtils fileStorageUtils;

    @MockBean
    private ExternalConfiguration externalConfiguration;

    @Before
    public void setUp() {
        Mockito.when(externalConfiguration.getRelativePathByStoragePath(Mockito.anyString())).thenReturn(System.getProperty("user.dir")+"/src/test/java/com/tsbatistatecnologia/preventseniorapi/testFiles/pathTest/access.log");
    }

    @Test
    public void save() throws Exception{
        MultipartFile multipartFile =  createMultipartFile();
        File file = fileStorageUtils.save(multipartFile.getName(),multipartFile.getInputStream());
        assertTrue(file.exists());
        file.delete();
    }

    private MultipartFile createMultipartFile(){

        Path path = Paths.get(System.getProperty("user.dir")+"/src/test/java/com/tsbatistatecnologia/preventseniorapi/testFiles/access.log");
        String name = "access.log";
        String originalFileName = "access.log";
        String contentType = "text/plain";
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile result = new MockMultipartFile(name,
                originalFileName, contentType, content);
        return result;
    }

}
