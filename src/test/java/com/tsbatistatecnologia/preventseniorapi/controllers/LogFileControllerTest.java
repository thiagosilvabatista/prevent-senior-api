package com.tsbatistatecnologia.preventseniorapi.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import com.tsbatistatecnologia.preventseniorapi.services.LogFileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LogFileControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogFileService logFileService;

    private static final String URL_BASE = "/api/log-file";
    private static final Long NUM_ID = 1L;

    @Before
    public void setUp() {
        LogFile logFile = createFileLog();
        Mockito.when(logFileService.update(Mockito.any(LogFile.class))).thenReturn(logFile);
        Mockito.when(logFileService.findLogsBetweenDatesAndIp(Mockito.any(String.class),
                                                              Mockito.any(String.class),
                                                              Mockito.any(String.class)
                                                             )).thenReturn(Arrays.asList(logFile));
        Mockito.when(this.logFileService.saveLog(Mockito.any(LogFile.class))).thenReturn(logFile);
        Mockito.when(logFileService.saveLogInBatch(Mockito.any(MultipartFile.class))).thenReturn(Arrays.asList(logFile));
        Mockito.when(logFileService.findById(NUM_ID)).thenReturn(Optional.of(logFile));
    }

    @Test
    public void saveLogs() throws Exception {
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file","access.log",
                "text/plain", "test data".getBytes());
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart(URL_BASE + File.separator +"import")
                        .file(mockMultipartFile);
        this.mvc.perform(builder).andExpect(status().isCreated())
                .andExpect(jsonPath("$[0].strIp").value("192.168.234.82"))
                .andExpect(jsonPath("$[0].strRequest").value("GET / HTTP/1.1"))
                .andExpect(jsonPath("$[0].numStatus").value(200))
                .andExpect(jsonPath("$[0].strUserAgent").value("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void saveLog() throws Exception {
        mvc.perform(post(URL_BASE).content(jsonLogFile()).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(jsonPath("$.strIp").value("192.168.234.82"))
                .andExpect(jsonPath("$.strRequest").value("GET / HTTP/1.1"))
                .andExpect(jsonPath("$.numStatus").value(200))
                .andExpect(jsonPath("$.strUserAgent").value("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"));
    }

    @Test
    public void findLogsBetweenDates() throws Exception {
       mvc.perform(get(URL_BASE + "/filter")
                .param("strIp","192.168.234.82")
                .param("startDate","2020-12-31 00:00:00.000")
                .param("endDate","2020-12-31 23:59:59.999")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].strIp").value("192.168.234.82"))
                .andExpect(jsonPath("$[0].strRequest").value("GET / HTTP/1.1"))
                .andExpect(jsonPath("$[0].numStatus").value(200))
                .andExpect(jsonPath("$[0].strUserAgent").value("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"));
    }

    @Test
    public void findById() throws Exception
    {
        mvc.perform(get(URL_BASE + File.separator + NUM_ID).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.strIp").value("192.168.234.82"))
                .andExpect(jsonPath("$.strRequest").value("GET / HTTP/1.1"))
                .andExpect(jsonPath("$.numStatus").value(200))
                .andExpect(jsonPath("$.strUserAgent").value("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"));
    }

    @Test
    public void deleteLogFile() throws Exception
    {
        mvc.perform(delete(URL_BASE + File.separator +NUM_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateLogFile() throws Exception
    {
        this.mvc.perform(put(URL_BASE).content(jsonLogFile())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.strIp").value("192.168.234.82"))
                .andExpect(jsonPath("$.strRequest").value("GET / HTTP/1.1"))
                .andExpect(jsonPath("$.numStatus").value(200))
                .andExpect(jsonPath("$.strUserAgent").value("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0"));
    }


    private String jsonLogFile() throws Exception
    {
        LogFile logFile = createFileLog();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(logFile);
    }


    private LogFile createFileLog() {
        final LogFile logFile =  new LogFile();
        logFile.setStrIp("192.168.234.82");
        logFile.setStrRequest("GET / HTTP/1.1");
        logFile.setNumStatus(200);
        logFile.setStrUserAgent("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0");
        return logFile;
    }

}
