package com.tsbatistatecnologia.preventseniorapi.repositories;

import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class LogFileRepositoryTest {

    @Autowired
    private LogFileRepository logFileRepository;

    private static final Long NUM_ID = 1L;
    private static final String STR_IP = "192.168.234.82";
    private static final LocalDateTime STR_START_DATE = LocalDateTime.parse("2019-01-01 00:00:00.000", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
    private static final LocalDateTime STR_END_DATE =  LocalDateTime.parse("2019-01-01 23:59:59.999", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));

    @Before
    public void setUp() {
        LogFile logFile = createFileLog();
        logFileRepository.save(logFile);
    }

    @Test
    public void findLogsByIp() throws Exception{
        Optional<List<LogFile>> logFiles = logFileRepository.findLogsByIp(STR_IP);
        assertTrue(logFiles.isPresent());
    }

    @Test
    public void findLogsBetweenDatesAndIp() throws Exception{
        Optional<List<LogFile>> logFiles = logFileRepository.findLogsBetweenDatesAndIp(STR_IP, STR_START_DATE, STR_END_DATE);
        assertTrue(logFiles.isPresent());
    }

    private LogFile createFileLog() {

        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        final LogFile logFile =  new LogFile();
        logFile.setStrIp("192.168.234.82");
        logFile.setStrRequest("GET / HTTP/1.1");
        logFile.setNumStatus(200);
        logFile.setStrUserAgent("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0");
        logFile.setDateData(LocalDateTime.parse("2019-01-01 11:11:11.111", formatter));
        return logFile;
    }

}
