package com.tsbatistatecnologia.preventseniorapi.services;

import com.tsbatistatecnologia.preventseniorapi.entities.LogFile;
import com.tsbatistatecnologia.preventseniorapi.repositories.LogFileRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class LogFileServiceTest {

    @MockBean
    private LogFileRepository logFileRepository;

    @Autowired
    private LogFileService logFileService;

    private static final Long NUM_ID = 1L;
    private static final String STR_IP = "192.168.234.82";
    private static final String STR_START_DATE = "2019-01-01 00:00:00.000";
    private static final String STR_END_DATE = "2019-01-01 23:59:59.999";

    @Before
    public void setUp() {
        Mockito.when(logFileRepository.findLogsBetweenDatesAndIp(Mockito.anyString(),
                                                                 Mockito.any(LocalDateTime.class),
                                                                 Mockito.any(LocalDateTime.class)))
                     .thenReturn(Optional.of(Arrays.asList(new LogFile())));
        Mockito.when(logFileRepository.findLogsByIp(Mockito.anyString())).thenReturn(Optional.of(Arrays.asList(new LogFile())));
        Mockito.when(logFileRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(new LogFile()));

        Mockito.when(logFileRepository.save(Mockito.any(LogFile.class))).thenReturn(new LogFile());

        Mockito.when(logFileRepository.saveAll(Mockito.anyList())).thenReturn(Arrays.asList(new LogFile()));
    }

    @Test
    public void findLogsBetweenDatesAndIp() throws Exception{
        List<LogFile> logFiles = logFileService.findLogsBetweenDatesAndIp(STR_IP, STR_START_DATE, STR_END_DATE);
        assertNotNull(logFiles);
    }

    @Test
    public void findLogsByIp() throws Exception{
        List<LogFile> logFiles = logFileService.findLogsByIp(STR_IP);
        assertNotNull(logFiles);
    }

    @Test
    public void findById() throws Exception{
        Optional<LogFile> logFile = logFileService.findById(NUM_ID);
        assertTrue(logFile.isPresent());
    }

    @Test
    public void saveLog() throws Exception{
        LogFile logFile = logFileService.saveLog(new LogFile());
        assertNotNull(logFile);
    }

    @Test
    public void saveAllLogs() throws Exception{
        List<LogFile> logFile = logFileService.saveAllLogs(Arrays.asList(new LogFile()));
        assertNotNull(logFile);
    }

    @Test
    public void update() throws Exception{
        LogFile logFile = logFileService.update(new LogFile());
        assertNotNull(logFile);
    }
}
