# Back End
código desenvolvido usando:
- Java 8
- Spring
- flyway

# Banco de dados
- Banco de dados Postgres

# Docker
- Na raiz do projeto dentro da pasta docker existe o arquivo docker-compose.yml
esse arquivo deve ser utilizado para subir o banco de dados.
- Para executar o arquivo docker-compose.yml acesse a pasta onde esta o arquivo
- Execute o comando sudo "docker-compose up -d"
- Depois que executar o comando acima o PgAdmin estara disponivel em http://localhost:16543/
- Para acessar o PgAdmin no campo "Email Address" adicione "http://localhost:16543/" e no campo password adicione "1234"
- Para acessar a instância do banco de dados no campo host name/Address adicione "postgres-compose"
User: root
Senha: 1234
- Depois disso basta subir a aplicação que o flyway ira criar as tabelas e views
